//
//  main.m
//  json
//
//  Created by clicklabs124 on 10/29/15.
//  Copyright (c) 2015 koshal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
