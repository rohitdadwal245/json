//
//  ViewController.m
//  json
//
//  Created by clicklabs124 on 10/29/15.
//  Copyright (c) 2015 koshal. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *latitudeLabel;
@property (weak, nonatomic) IBOutlet UILabel *longitudeLabel;
@property (weak, nonatomic) IBOutlet UILabel *tempMax;
@property (weak, nonatomic) IBOutlet UILabel *tempMin;
@property (weak, nonatomic) IBOutlet UILabel *pressure;
@property (weak, nonatomic) IBOutlet UILabel *latLabel;
@property (weak, nonatomic) IBOutlet UILabel *longLabel;
@property (weak, nonatomic) IBOutlet UILabel *tempreatureMaxLabel;
@property (weak, nonatomic) IBOutlet UILabel *tempreatureMinLabel;
@property (weak, nonatomic) IBOutlet UILabel *pressureLabel;

@end

@implementation ViewController
@synthesize latitudeLabel;
@synthesize longitudeLabel;
@synthesize tempMax;
@synthesize tempMin;
@synthesize pressure;
@synthesize latLabel;
@synthesize longLabel;
@synthesize tempreatureMaxLabel;
@synthesize tempreatureMinLabel;
@synthesize pressureLabel;
- (void)viewDidLoad {
    [super viewDidLoad];

    
    
    NSString*jsondata =[[NSBundle mainBundle]pathForResource:@"jsontxt" ofType:@"json"];
    NSData*data =[NSData dataWithContentsOfFile:jsondata];
    NSDictionary*json =[NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    NSLog(@"%@",json);
    NSDictionary*coord =json[@"coord"];
    NSString*lon = coord[@"lon"];
    NSString*lat =coord[@"lat"];
    NSLog(@"%@",lon);
    NSLog(@"%@",lat);
    NSArray *weather =json[@"weather"];
    for (int i=0; i<weather.count; i++){
        NSDictionary*json2 =weather[i];
        NSString*main = json2[@"main"];
        NSLog(@"%@",main);
        NSString *description=json2[@"description"];
        NSString *icon=json2[@"icon"];
        NSLog(@"%@",description);
        NSLog(@"%@",icon);
        NSString*id=json2[@"id"];
        NSLog(@"%@",id);
    }
         NSDictionary*main=json[@"main"];
         NSString *temp=main[@"temp"];
        NSString *pressure=main[@"pressure"];
        NSString *humidity=main[@"humidity"];
        NSString *temp_min=main[@"temp_min"];
        NSString *temp_max=main[@"temp_max"];
        NSLog(@"%@",temp);
        NSLog(@"%@",pressure);
        NSLog(@"%@",humidity);
        NSLog(@"%@",temp_min);
        NSLog(@"%@",temp_max);
   latLabel.text=[NSString stringWithFormat:@"%@",lat];
     longLabel.text=[NSString stringWithFormat:@"%@",lon];
     tempreatureMaxLabel.text=[NSString stringWithFormat:@"%@",temp_max];
     tempreatureMinLabel.text=[NSString stringWithFormat:@"%@",temp_min];
     pressureLabel.text=[NSString stringWithFormat:@"%@",pressure];
    
    
                       
    
        
    



    
    
    
    
    // Do any additional setup after loading the view, typically from a nib.
  
    
    
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
